export const localStoragePrefix = "SHOPPING_LIST";

export const setLocalStorageItem = <T extends unknown>(
  key: string,
  value: T
) => {
  if (typeof window == "undefined") {
    console.warn(
      `Tried setting localStorage key “${key}” even though environment is not a client`
    );
  }

  try {
    // Save to local storage
    window.localStorage.setItem(
      `${localStoragePrefix}--${key}`,
      JSON.stringify(value)
    );

    // We dispatch a custom event so every useLocalStorage hook are notified
    window.dispatchEvent(new Event("local-storage"));
  } catch (error) {
    console.warn(`Error setting localStorage key “${key}”:`, error);
  }
};

export const getLocalStorageItem = <T extends unknown>(
  key: string
): T | undefined => {
  if (typeof window === "undefined") {
    return undefined;
  }

  try {
    const item = window.localStorage.getItem(`${localStoragePrefix}--${key}`);
    return item ? (JSON.parse(item) as T) : undefined;
  } catch (error) {
    console.warn(`Error reading localStorage key “${key}”:`, error);
    return undefined;
  }
};

export const removeLocalStorageItem = (key: string) => {
  if (typeof window === "undefined") {
    return undefined;
  }

  try {
    window.localStorage.removeItem(`${localStoragePrefix}--${key}`);
  } catch (error) {
    console.warn(`Error reading localStorage key “${key}”:`, error);
  }
};
