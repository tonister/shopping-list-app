import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import listReducer from "../pages/list/listSlice";
import historyReducer from "../pages/history/historySlice";

export const store = configureStore({
  reducer: {
    list: listReducer,
    history: historyReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
