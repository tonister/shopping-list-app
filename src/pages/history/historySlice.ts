import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  getLocalStorageItem,
  removeLocalStorageItem,
  setLocalStorageItem,
} from "../../utils/localStorage";
import { AppThunk, RootState } from "../../app/store";

export interface HistoryItemDTO {
  id: string;
  title: string;
}

export interface HistoryDTO {
  items: HistoryItemDTO[];
}

const initialState: HistoryDTO = {
  items: [],
};

export const historySlice = createSlice({
  name: "history",
  initialState,
  reducers: {
    initHistory: (state) => {
      const historyData: HistoryDTO | undefined =
        getLocalStorageItem("history");
      if (historyData) {
        state.items = historyData.items;
      }
    },
    addList: (state, action: PayloadAction<HistoryItemDTO>) => {
      const foundReference = state.items.find(
        ({ id }) => id === action.payload.id
      );
      if (!foundReference) {
        state.items = [...state.items, action.payload];
        setLocalStorageItem("history", state);
      }
    },
    updateLists: (state, action: PayloadAction<HistoryDTO>) => {
      state.items = action.payload.items;
    },
  },
});

export const selectHistoryItems = (state: RootState) => state.history.items;

export const { initHistory, addList, updateLists } = historySlice.actions;

export default historySlice.reducer;

export const removeList =
  (id: string): AppThunk =>
  (dispatch, getState) => {
    const state: RootState = getState();
    const newHistory: HistoryDTO = {
      ...state.history,
      items: state.history.items.filter((item) => item.id !== id),
    };
    dispatch(updateLists(newHistory));
    setLocalStorageItem("history", newHistory);
    removeLocalStorageItem(`list__${id}`);
  };

export const updateList =
  (item: HistoryItemDTO): AppThunk =>
  (dispatch, getState) => {
    const state: RootState = getState();
    const newHistory: HistoryDTO = {
      ...state.history,
    };
    newHistory.items = state.history.items.map((i) => {
      if (i.id === item.id) {
        return item;
      }
      return i;
    });
    state.history = newHistory;
    setLocalStorageItem("history", newHistory);
  };
