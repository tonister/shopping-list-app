import React, { CSSProperties } from "react";
import { Link } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { initHistory, removeList, selectHistoryItems } from "./historySlice";
import { resetList } from "../list/listSlice";

const historyListStyle: CSSProperties = {
  display: "flex",
  flexWrap: "wrap",
  width: "100%",
};

const historyItemStyle: CSSProperties = {
  border: "none",
  padding: 12,
  margin: 15,
  boxShadow: "2px 2px 4px grey",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  borderRadius: 5,
  width: "calc(100% / 4 - 2 * 15px)",
  backgroundColor: "oldlace",
};

const removeButtonStyles: CSSProperties = {
  marginTop: 15,
};

const HistoryView = () => {
  const dispatch = useAppDispatch();
  const historyItems = useAppSelector(selectHistoryItems);

  React.useEffect(() => {
    dispatch(initHistory());
  }, [dispatch]);

  return (
    <main>
      <h2>HistoryView</h2>
      <Link to={"/list"} onClick={() => dispatch(resetList())}>
        Create new list
      </Link>
      <div className={"history-list"} style={historyListStyle}>
        {historyItems.map((item) => (
          <div
            className={"history-list__item"}
            style={historyItemStyle}
            key={item.id}
          >
            <h3>{item.title}</h3>
            <Link to={`/list/${item.id}`}>Open list</Link>
            <button
              style={removeButtonStyles}
              type="button"
              onClick={() => dispatch(removeList(item.id))}
            >
              Remove list
            </button>
          </div>
        ))}
      </div>
    </main>
  );
};

export default React.memo(HistoryView);
