import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { v4 as uuidv4 } from "uuid";
import {
  getLocalStorageItem,
  setLocalStorageItem,
} from "../../utils/localStorage";
import { AppThunk, RootState } from "../../app/store";

export interface ListItemDTO {
  id: string;
  value: string;
  checked: boolean;
}

export interface ListDTO {
  id: string;
  title: string;
  items: ListItemDTO[];
}

const initialState: ListDTO = {
  id: "",
  title: "New list",
  items: [
    {
      id: "",
      value: "",
      checked: false,
    },
  ],
};

export const fetchListData = createAsyncThunk(
  "list/fetchAsync",
  async (id: string, thunkAPI) => {
    return new Promise<ListDTO>((resolve) => {
      const data: ListDTO | undefined = getLocalStorageItem(
        `list__${id}`
      ) as ListDTO;
      resolve(data);
    });
  }
);

export const listSlice = createSlice({
  name: "list",
  initialState,
  reducers: {
    initList: (state, action: PayloadAction<ListDTO>) => {
      const { id, title, items } = action.payload;
      state.id = id;
      state.title = title;
      state.items = [
        ...items,
        {
          id: "",
          value: "",
          checked: false,
        },
      ];
    },
    resetList: (state) => {
      state.id = initialState.id;
      state.title = initialState.title;
      state.items = [...initialState.items];
    },
    updateTitle: (state, action: PayloadAction<string>) => {
      state.title = action.payload;
    },
    addListItem: (state, action: PayloadAction<ListItemDTO>) => {
      state.items = [
        ...state.items.filter(({ id }) => id !== ""),
        {
          ...action.payload,
          id: uuidv4(),
        },
        {
          id: "",
          value: "",
          checked: false,
        },
      ];
    },
    toggleItemChecked: (state, action: PayloadAction<string>) => {
      state.items.forEach((item) => {
        if (item.id === action.payload) {
          item.checked = !item.checked;
        }
      });
    },
    updateListItemValue: (
      state,
      action: PayloadAction<{
        id: string;
        value: string;
      }>
    ) => {
      state.items.forEach((item) => {
        if (item.id === action.payload.id) {
          item.value = action.payload.value;
        }
      });
    },
    removeListItem: (state, action: PayloadAction<string>) => {
      state.items = state.items.filter(({ id }) => id !== action.payload);
    },
    updateListId: (state, action: PayloadAction<string>) => {
      state.id = action.payload;
    },
  },
});

export const selectListData = (state: RootState) => state.list;

export const {
  initList,
  resetList,
  addListItem,
  removeListItem,
  toggleItemChecked,
  updateListItemValue,
  updateTitle,
} = listSlice.actions;

export default listSlice.reducer;

export const saveList =
  (): AppThunk<string> =>
  (dispatch, getState): string => {
    const state: ListDTO = getState().list;
    const id = state.id ? state.id : uuidv4();
    // remove empty items
    const items = state.items.filter((item) => !!item.value && !!item.id);
    dispatch(listSlice.actions.updateListId(id));
    setLocalStorageItem(`list__${id}`, { ...state, id, items });
    return id;
  };
