import { ListDTO } from "./listSlice";
import { HistoryDTO } from "../history/historySlice";

export const mockListData1: ListDTO = {
  id: "42d0904b-ef5b-4a90-87ce-6e84aafc0429",
  title: "Existing list 1",
  items: [
    {
      id: "746b88b9-e657-415c-8a14-08cc62e5ec01",
      value: "List item 1",
      checked: false,
    },
    {
      id: "693815ba-af4b-4aab-a60a-e5ded5884615",
      value: "List item 2",
      checked: false,
    },
    {
      id: "7f90ea6a-3f97-47ae-a508-57a30f25ee16",
      value: "List item 3",
      checked: false,
    },
    {
      id: "3b44ae7c-2484-4d6b-b949-a3351734d7cb",
      value: "List item 4",
      checked: false,
    },
    {
      id: "ae1c0f3d-5ecc-4705-a3e2-810797f5afee",
      value: "List item 5",
      checked: false,
    },
  ],
};

export const mockListData2: ListDTO = {
  id: "87531bb6-ad69-4998-9344-27acb1d6f10f",
  title: "Existing list 2",
  items: [
    {
      id: "ef78256b-3aab-4e86-aa22-bca32c5d96ec",
      value: "Checked item 1",
      checked: true,
    },
    {
      id: "4da3a9bb-0878-4ff5-9673-cfc0823067b6",
      value: "Unchecked item 1",
      checked: false,
    },
    {
      id: "74092f77-92db-4c11-9191-68ecfaa4ec5d",
      value: "Unchecked item 2",
      checked: false,
    },
    {
      id: "7d0654c0-9852-4714-ae1b-299607c430a9",
      value: "Checked item 2",
      checked: true,
    },
  ],
};

export const mockHistoryData: HistoryDTO = {
  items: [
    { id: "42d0904b-ef5b-4a90-87ce-6e84aafc0429", title: "Existing list 1" },
    { id: "87531bb6-ad69-4998-9344-27acb1d6f10f", title: "Existing list 2" },
  ],
};
