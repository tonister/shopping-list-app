import React, { ChangeEvent, CSSProperties } from "react";
import { Field, FieldArray, Form, Formik } from "formik";
import {
  addListItem,
  fetchListData,
  initList,
  ListDTO,
  removeListItem,
  saveList,
  selectListData,
  toggleItemChecked,
  updateListItemValue,
  updateTitle,
} from "./listSlice";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { useHistory, useParams } from "react-router";
import {
  addList,
  initHistory,
  selectHistoryItems,
  updateList,
} from "../history/historySlice";

const listTitleStyles: CSSProperties = {
  fontSize: 18,
  outline: "none",
  border: "none",
  borderBottom: "1px solid gray",
  marginBottom: 25,
  marginRight: 20,
};

const listItemStyles: CSSProperties = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
  marginBottom: 15,
};

const itemCheckboxStyles: CSSProperties = {
  width: 24,
  height: 24,
};

const itemInputStyles: CSSProperties = {
  height: 24,
  margin: "0 15px",
};

const itemButtonStyles: CSSProperties = {
  width: 100,
  height: 24,
};

export interface ListViewParams {
  id?: string;
}

const ListView = () => {
  const dispatch = useAppDispatch();
  const history = useHistory();
  const { id } = useParams<ListViewParams>();
  const currentListData = useAppSelector(selectListData);
  const historyData = useAppSelector(selectHistoryItems);

  const fetchList = React.useCallback(async () => {
    if (id) {
      const { payload } = await dispatch(fetchListData(id));
      if (!payload) {
        history.push("/list");
      } else {
        dispatch(initList(payload as ListDTO));
      }
    }
  }, [id, history, dispatch]);

  React.useEffect(() => {
    if (historyData.length === 0) {
      dispatch(initHistory());
    }
  });

  React.useEffect(() => {
    if (!id && currentListData.id) {
      history.push(`/list/${currentListData.id}`);
    }
    if (id) {
      fetchList();
    }
  }, [id, fetchList, history, currentListData.id]);

  const handleSubmit = async () => {
    const listId: string = await dispatch(saveList());

    if (id) {
      dispatch(
        updateList({ id: currentListData.id, title: currentListData.title })
      );
    }
    if (!id && listId) {
      dispatch(addList({ id: listId, title: currentListData.title }));
    }
  };

  return (
    <main>
      <h2>ListView</h2>
      <Formik
        initialValues={currentListData}
        enableReinitialize
        onSubmit={handleSubmit}
      >
        {(props) => (
          <Form>
            <Field
              type="text"
              name="title"
              style={listTitleStyles}
              onBlur={() => dispatch(updateTitle(props.values.title))}
            />
            <button type="submit" style={itemButtonStyles}>
              Save list
            </button>
            <FieldArray name="items">
              {() => (
                <div>
                  {props.values.items.map((item, index) => (
                    <div key={index} style={listItemStyles}>
                      <Field
                        name={`items[${index}].checked`}
                        type="checkbox"
                        style={itemCheckboxStyles}
                        onChange={() => dispatch(toggleItemChecked(item.id))}
                      />
                      <Field
                        name={`items[${index}].value`}
                        type="text"
                        style={itemInputStyles}
                        placeholder="New list item"
                        onBlur={(evt: ChangeEvent<HTMLInputElement>) =>
                          dispatch(
                            updateListItemValue({
                              id: item.id,
                              value: evt.currentTarget.value,
                            })
                          )
                        }
                      />
                      {item.id ? (
                        <button
                          style={itemButtonStyles}
                          type="button"
                          onClick={() => dispatch(removeListItem(item.id))}
                        >
                          Remove
                        </button>
                      ) : (
                        <button
                          style={itemButtonStyles}
                          type="button"
                          onClick={() =>
                            dispatch(addListItem(props.values.items[index]))
                          }
                        >
                          Add item
                        </button>
                      )}
                    </div>
                  ))}
                </div>
              )}
            </FieldArray>
          </Form>
        )}
      </Formik>
    </main>
  );
};

export default React.memo(ListView);
