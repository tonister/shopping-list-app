import { render, screen } from "@testing-library/react";
import { createMemoryHistory } from "history";
import Header from "./Header";
import { Provider } from "react-redux";
import { store } from "../../app/store";
import { Router } from "react-router";
import userEvent from "@testing-library/user-event";

describe("Header", () => {
  it("should render", () => {
    const history = createMemoryHistory();
    render(
      <Provider store={store}>
        <Router history={history}>
          <Header />
        </Router>
      </Provider>
    );
    expect(screen.getByText(/Dashboard/i)).toBeInTheDocument();
    expect(screen.getByText(/New List/i)).toBeInTheDocument();
    expect(screen.getByText(/History/i)).toBeInTheDocument();
  });
  it("should navigate to history page", () => {
    const history = createMemoryHistory();
    render(
      <Provider store={store}>
        <Router history={history}>
          <Header />
        </Router>
      </Provider>
    );
    userEvent.click(screen.getByText(/History/i));
    expect(screen.getByText(/History/i)).toBeInTheDocument();
  });
  it("should navigate to list page", () => {
    const history = createMemoryHistory();
    render(
      <Provider store={store}>
        <Router history={history}>
          <Header />
        </Router>
      </Provider>
    );
    userEvent.click(screen.getByText(/New List/i));
    expect(screen.getByText(/New List/i)).toBeInTheDocument();
  });
});
