import React, { CSSProperties } from "react";
import { Link } from "react-router-dom";
import { useAppDispatch } from "../../app/hooks";
import { resetList } from "../../pages/list/listSlice";

const headerStyles: CSSProperties = {
  width: "100%",
  display: "flex",
  flexDirection: "row",
  margin: "0 auto",
  justifyContent: "center",
  position: "sticky",
  top: 0,
  zIndex: 20,
  background: "#fff",
};

const headerItemStyles: CSSProperties = {
  padding: 15,
};

const Header = () => {
  const dispatch = useAppDispatch();
  const routes: Array<{
    path: string;
    title: string;
    onClick?: () => void;
  }> = [
    {
      path: "/",
      title: "Dashboard",
    },
    {
      path: "/history",
      title: "History",
    },
    {
      path: "/list",
      title: "New List",
      onClick: () => {
        dispatch(resetList());
      },
    },
  ];
  return (
    <header style={headerStyles}>
      {routes.map(({ path, title, onClick }) => (
        <Link style={headerItemStyles} key={path} to={path} onClick={onClick}>
          {title}
        </Link>
      ))}
    </header>
  );
};

export default React.memo(Header);
