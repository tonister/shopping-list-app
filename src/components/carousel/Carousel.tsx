import React from "react";
import { FaArrowAltCircleRight, FaArrowAltCircleLeft } from "react-icons/fa";

import "./carousel.css";
import clsx from "clsx";

export interface CarouselProps {
  slides: SlideProps[];
}

const Carousel = ({ slides }: CarouselProps) => {
  const [current, setCurrent] = React.useState(0);
  const length = slides.length;
  const prevSlide = () => {
    setCurrent(current === 0 ? length - 1 : current - 1);
  };
  const nextSlide = () => {
    setCurrent(current === length - 1 ? 0 : current + 1);
  };
  if (!Array.isArray(slides) || slides.length <= 0) {
    return null;
  }

  return (
    <section className="slider">
      <FaArrowAltCircleLeft
        className="slider__prev left-arrow"
        onClick={prevSlide}
      />
      <FaArrowAltCircleRight
        className="slider__next right-arrow"
        onClick={nextSlide}
      />
      {slides.map((slide, index) => (
        <Slide
          className={clsx({
            next: current === length - 1 ? index === 0 : index === current + 1,
            previous:
              current === 0 ? index === length - 1 : index === current - 1,
            active: current === index,
          })}
          key={index}
          {...slide}
        />
      ))}
    </section>
  );
};

export default React.memo(Carousel);

export interface SlideProps {
  image: string | JSX.Element;
  title?: string;
  description?: string;
  className?: string;
}

const Slide = ({ image, title, description, className }: SlideProps) => {
  if (typeof image === "string") {
    return (
      <div className={clsx("slide", className)}>
        <img src={image} alt={title || description} className="slider__image" />
      </div>
    );
  }
  return <image />;
};
