import Image1 from "../../assets/images/colin-lloyd-NKuE1PGiNbw-unsplash.jpg";
import Image2 from "../../assets/images/dennis-buchner-2hKHc5CeuxI-unsplash.jpg";
import Image3 from "../../assets/images/jeremy-bishop-ph5AyjFltQU-unsplash.jpg";

export const slides = [
  {
    title: "1",
    description: "",
    image: Image1,
  },
  {
    title: "2",
    description: "",
    image: Image2,
  },
  {
    title: "3",
    description: "",
    image: Image3,
  },
];
