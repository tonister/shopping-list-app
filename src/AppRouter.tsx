import { Redirect, Route, Switch } from "react-router";
import React from "react";
import Header from "./components/header/Header";

const ListView = React.lazy(() => import("./pages/list/ListView"));
const HistoryView = React.lazy(() => import("./pages/history/HistoryView"));
const LandingPage = React.lazy(() => import("./pages/landing/LandingPage"));

const AppRouter = () => {
  return (
    <>
      <Header />
      <Switch>
        <Route exact path="/" component={LandingPage} />
        <Route exact path="/history" component={HistoryView} />
        <Route exact path="/list" component={ListView} />
        <Route path="/list/:id?" component={ListView} />
        <Route>
          <Redirect to="/" />
        </Route>
      </Switch>
    </>
  );
};

export default React.memo(AppRouter);
