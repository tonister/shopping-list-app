import { render, screen } from "@testing-library/react";
import { createMemoryHistory } from "history";
import AppRouter from "./AppRouter";
import { Router } from "react-router";
import { Provider } from "react-redux";
import { store } from "./app/store";

describe("AppRouter", () => {
  it("should display landingPage by default", () => {
    const history = createMemoryHistory();
    render(
      <Provider store={store}>
        <Router history={history}>
          <AppRouter />
        </Router>
      </Provider>
    );
    expect(screen.getByText(/LandingPage/i)).toBeInTheDocument();
  });
  it("should redirect to landing page when bad url is inserted", () => {
    const history = createMemoryHistory();
    history.push("/badUrl");
    render(
      <Provider store={store}>
        <Router history={history}>
          <AppRouter />
        </Router>
      </Provider>
    );
    expect(screen.getByText(/LandingPage/i)).toBeInTheDocument();
  });
  it("should display history page", () => {
    const history = createMemoryHistory();
    history.push("/history");
    render(
      <Provider store={store}>
        <Router history={history}>
          <AppRouter />
        </Router>
      </Provider>
    );
    expect(screen.getByText(/HistoryView/i)).toBeInTheDocument();
  });
  it("should display list page", () => {
    const history = createMemoryHistory();
    history.push("/list");
    render(
      <Provider store={store}>
        <Router history={history}>
          <AppRouter />
        </Router>
      </Provider>
    );
    expect(screen.getByText(/ListView/i)).toBeInTheDocument();
  });
});
