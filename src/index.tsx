import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { store } from "./app/store";
import { Provider } from "react-redux";
import * as serviceWorker from "./serviceWorker";
import AppRouter from "./AppRouter";

import "./styles.css";
import { localStoragePrefix, setLocalStorageItem } from "./utils/localStorage";
import {
  mockHistoryData,
  mockListData1,
  mockListData2,
} from "./pages/list/mockData";
import { BrowserRouter } from "react-router-dom";

const RootComponent = () => {
  const init = () => {
    const localStorageKeys = Object.keys(window.localStorage.valueOf());
    if (!localStorageKeys.some((key) => key.match(localStoragePrefix))) {
      setLocalStorageItem("history", mockHistoryData);
      setLocalStorageItem(`list__${mockListData1.id}`, mockListData1);
      setLocalStorageItem(`list__${mockListData2.id}`, mockListData2);
    }
  };
  React.useEffect(init);
  return (
    <React.StrictMode>
      <Provider store={store}>
        <React.Suspense fallback={<span>Loading ...</span>}>
          <BrowserRouter>
            <AppRouter />
          </BrowserRouter>
        </React.Suspense>
      </Provider>
    </React.StrictMode>
  );
};

ReactDOM.render(<RootComponent />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
